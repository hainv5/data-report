package org.prox.demo1.specification;

import org.prox.demo1.dto.request.OfferFilterForm;
import org.prox.demo1.entity.OfferDataMint;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class OfferSpecification {
    public static Specification<OfferDataMint> buildSpec(OfferFilterForm form) {
        return form == null ? null : new Specification<OfferDataMint>() {
            @Override
            public Predicate toPredicate(Root<OfferDataMint> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                List<Predicate> predicates = new ArrayList<>();



                var id = form.getOfferDataMintId();
                if(id != null) {
                    var predicate = builder.equal(root.get("id"),id);
                    predicates.add(predicate);
                }

                return builder.and(predicates.toArray(new Predicate[0]));
            }
        };
    }
}
