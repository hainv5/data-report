package org.prox.demo1.dto.request;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OfferDataMintRequest {
    @Column(name = "campain_id")
    private String campaignId;
    private String click;
    private String conversion;
    private String cpc;
    private String ctr;
    private String currency;
    private String cvr;
    private String date;
    private String ecpm;
    private String impression;
    private String ivr;
    private String location;
    @Column(name = "offer_id")
    private String offerId;
    @Column(name = "offer_name")
    private String offerName;
    @Column(name = "offer_uuid")
    private String offerUuid;
    private String spend;
}
