package org.prox.demo1.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OfferFilterForm {
    private Long OfferDataMintId;
}
