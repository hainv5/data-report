package org.prox.demo1.dto.response;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

import javax.naming.Name;

@Getter
@Setter
public class OfferDataMintResponse {
    private Long id;
    @Column(name = "campain_id")
    private String campaignId;
    private String click;
    private String conversion;
    private String cpc;
    private String ctr;
    private String currency;
    private String cvr;
    private String date;
    private String ecpm;
    private String impression;
    private String ivr;
    private String location;
    @Column(name = "offer_id")
    private String offerId;
    @Column(name = "offer_name")
    private String offerName;
    @Column(name = "offer_uuid")
    private String offerUuid;
    private String spend;
}
