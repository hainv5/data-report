package org.prox.demo1.service;

import org.prox.demo1.dto.request.OfferFilterForm;
import org.prox.demo1.dto.response.OfferDataMintResponse;
import org.prox.demo1.dto.request.OfferDataMintRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface OfferDataMintService {
    Page<OfferDataMintResponse> findAllOffers(OfferFilterForm form, Pageable pageable);

    OfferDataMintResponse create(OfferDataMintRequest request);
    OfferDataMintResponse update(Long id, OfferDataMintRequest request);
    void deleteById(Long id);
}
