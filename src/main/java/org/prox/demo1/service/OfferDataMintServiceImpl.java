package org.prox.demo1.service;

import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.prox.demo1.dto.request.OfferFilterForm;
import org.prox.demo1.dto.response.OfferDataMintResponse;
import org.prox.demo1.entity.OfferDataMint;
import org.prox.demo1.dto.request.OfferDataMintRequest;
import org.prox.demo1.repository.OfferDataMintRepository;
import org.prox.demo1.specification.OfferSpecification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class OfferDataMintServiceImpl implements OfferDataMintService{

    private final OfferDataMintRepository offerDataMintRepository;
    private final ModelMapper modelMapper;


    @Override
    public Page<OfferDataMintResponse> findAllOffers(OfferFilterForm form, Pageable pageable) {
        var spec = OfferSpecification.buildSpec(form);
        return offerDataMintRepository.findAll(spec, pageable)
                .map(offerDataMint -> modelMapper.map(offerDataMint, OfferDataMintResponse.class));
    }

    @Override
        public OfferDataMintResponse create(OfferDataMintRequest request) {
        var offer = new OfferDataMint();
        offer.setOfferId(request.getOfferId());
        offer.setCampaignId(request.getCampaignId());
        offer.setClick(request.getClick());
        offer.setConversion(request.getConversion());
        offer.setCpc(request.getCpc());
        offer.setCtr(request.getCtr());
        offer.setCurrency(request.getCurrency());
        offer.setCvr(request.getCvr());
        offer.setDate(request.getDate());
        offer.setEcpm(request.getEcpm());
        offer.setImpression(request.getImpression());
        offer.setIvr(request.getIvr());
        offer.setLocation(request.getLocation());
        offer.setOfferName(request.getOfferName());
        offer.setOfferUuid(request.getOfferUuid());
        offer.setSpend(request.getSpend());

        System.out.printf(offer.toString());
        var savedOffer = offerDataMintRepository.save(offer);
        var dto = new OfferDataMintResponse();
        dto.setId(savedOffer.getId());
        dto.setCampaignId(savedOffer.getCampaignId());
        dto.setClick(savedOffer.getClick());
        dto.setConversion(savedOffer.getConversion());
        dto.setCpc(savedOffer.getCpc());
        dto.setCtr(savedOffer.getCtr());
        dto.setCurrency(savedOffer.getCurrency());
        dto.setCvr(savedOffer.getCvr());
        dto.setDate(savedOffer.getDate());
        dto.setEcpm(savedOffer.getEcpm());
        dto.setImpression(savedOffer.getImpression());
        dto.setIvr(savedOffer.getIvr());
        dto.setLocation(savedOffer.getLocation());
        dto.setOfferId(savedOffer.getOfferId());
        dto.setOfferName(savedOffer.getOfferName());
        dto.setOfferUuid(savedOffer.getOfferUuid());
        dto.setSpend(savedOffer.getSpend());
        return dto;

//        OfferDataMint offerDataMint = modelMapper.map(request, OfferDataMint.class);
//        OfferDataMint savedOffer = offerDataMintRepository.save(offerDataMint);
//        return modelMapper.map(savedOffer, OfferDataMintResponse.class);

    }

    @Override
    public OfferDataMintResponse update(Long id, OfferDataMintRequest request) {
        OfferDataMint offer = offerDataMintRepository.findById(id).orElseThrow(() -> new RuntimeException("Offer not found"));
        offer.setCampaignId(request.getCampaignId());
        offer.setClick(request.getClick());
        offer.setConversion(request.getConversion());
        offer.setCpc(request.getCpc());
        offer.setCtr(request.getCtr());
        offer.setCurrency(request.getCurrency());
        offer.setCvr(request.getCvr());
        offer.setDate(request.getDate());
        offer.setEcpm(request.getEcpm());
        offer.setImpression(request.getImpression());
        offer.setIvr(request.getIvr());
        offer.setLocation(request.getLocation());
        offer.setOfferId(request.getOfferId());
        offer.setOfferName(request.getOfferName());
        offer.setOfferUuid(request.getOfferUuid());
        offer.setSpend(request.getSpend());

        OfferDataMint savedOffer = offerDataMintRepository.save(offer);

        OfferDataMintResponse dto = new OfferDataMintResponse();
        dto.setId(savedOffer.getId());
        dto.setCampaignId(savedOffer.getCampaignId());
        dto.setClick(savedOffer.getClick());
        dto.setConversion(savedOffer.getConversion());
        dto.setCpc(savedOffer.getCpc());
        dto.setCtr(savedOffer.getCtr());
        dto.setCurrency(savedOffer.getCurrency());
        dto.setCvr(savedOffer.getCvr());
        dto.setDate(savedOffer.getDate());
        dto.setEcpm(savedOffer.getEcpm());
        dto.setImpression(savedOffer.getImpression());
        dto.setIvr(savedOffer.getIvr());
        dto.setLocation(savedOffer.getLocation());
        dto.setOfferId(savedOffer.getOfferId());
        dto.setOfferName(savedOffer.getOfferName());
        dto.setOfferUuid(savedOffer.getOfferUuid());
        dto.setSpend(savedOffer.getSpend());

        return dto;
    }

    @Override
    public void deleteById(Long id) {
        offerDataMintRepository.deleteById(id);
    }
}
