package org.prox.demo1.repository;

import org.prox.demo1.entity.OfferDataMint;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface OfferDataMintRepository extends JpaRepository<OfferDataMint, Long>, JpaSpecificationExecutor<OfferDataMint> {
}
