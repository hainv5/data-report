package org.prox.demo1.controller;

import lombok.AllArgsConstructor;
import org.prox.demo1.dto.request.OfferFilterForm;
import org.prox.demo1.dto.response.OfferDataMintResponse;
import org.prox.demo1.dto.request.OfferDataMintRequest;
import org.prox.demo1.service.OfferDataMintService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@CrossOrigin("*")
public class OfferDataMintController {
    private final OfferDataMintService offerDataMintService;


    @GetMapping("api/v1/offerDataMint")
    public Page<OfferDataMintResponse> findAllOffers(OfferFilterForm form, Pageable pageable) {
        return offerDataMintService.findAllOffers(form, pageable);
    }

    @PostMapping("api/v1/offerDataMint")
    public OfferDataMintResponse create(@RequestBody OfferDataMintRequest request) {
        return offerDataMintService.create(request);
    }

    @PutMapping("api/v1/offerDataMint/{id}")
    public OfferDataMintResponse update(@PathVariable Long id, @RequestBody OfferDataMintRequest request) {
        return offerDataMintService.update(id, request);
    }

    @DeleteMapping("api/v1/offerDataMint/{id}")
    public void deleteByid(@PathVariable Long id) {
        offerDataMintService.deleteById(id);
    }
}
