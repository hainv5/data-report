package org.prox.demo1.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "offer_data_mint")
public class OfferDataMint {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "campaign_id")
    private String campaignId;

    @Column(name = "click")
    private String click;

    @Column(name = "conversion")
    private String conversion;

    @Column(name = "cpc")
    private String cpc;

    @Column(name = "ctr")
    private String ctr;

    @Column(name = "currency")
    private String currency;

    @Column(name = "cvr")
    private String cvr;

    @Column(name = "date")
    private String date;

    @Column(name = "ecpm")
    private String ecpm;

    @Column(name = "impression")
    private String impression;

    @Column(name = "ivr")
    private String ivr;

    @Column(name = "location")
    private String location;

    @Column(name = "offer_id")
    private String offerId;

    @Column(name = "offer_name")
    private String offerName;

    @Column(name = "offer_uuid")
    private String offerUuid;

    @Column(name = "spend")
    private String spend;

    @Override
    public String toString() {
        return "OfferDataMint{" +
                "id=" + id +
                ", campaignId='" + campaignId + '\'' +
                ", click='" + click + '\'' +
                ", conversion='" + conversion + '\'' +
                ", cpc='" + cpc + '\'' +
                ", ctr='" + ctr + '\'' +
                ", currency='" + currency + '\'' +
                ", cvr='" + cvr + '\'' +
                ", date='" + date + '\'' +
                ", ecpm='" + ecpm + '\'' +
                ", impression='" + impression + '\'' +
                ", ivr='" + ivr + '\'' +
                ", location='" + location + '\'' +
                ", offerId='" + offerId + '\'' +
                ", offerName='" + offerName + '\'' +
                ", offerUuid='" + offerUuid + '\'' +
                ", spend='" + spend + '\'' +
                '}';
    }
}
